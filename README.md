# Dotfiles for The Pins Team's Development Machines

## Bootstrap

* Setup your Linux VM using the server-optimized distro of your choice, preferrly Ubuntu Server with latest LTS version.
* If prompted to add your SSH keys during installation, skip. If the installer prompted you to set root password, leave it blank to use `sudo` instead. Just chill while the installer installs the server distro
* Once successfully logged in for the first time, run the following script:

```bash
# site in progress, use the next one
$(command -v curl>>/dev/null && echo curl -o- || echo wget -q0-) https://scripts.madebythepins.tk/dotfiles/bootstrap-devenv/debian-ubuntu

# while it's WIP, use me OwO
$(command -v curl>>/dev/null && echo curl -o- || echo wget -q0-) https://gitlab.com/MadeByThePinsHub/dotfiles/-/raw/master/bootstrap-linux.sh
```
